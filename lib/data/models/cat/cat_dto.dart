import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/cat_entity.dart';

part 'cat_dto.g.dart';
part 'cat_dto.freezed.dart';

@freezed
abstract class CatDto implements _$CatDto {
  const CatDto._();

  @JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
  const factory CatDto({
    required String? id,
    required String? name,
    required String? origin,
    required int? intelligence,
    required int? healthIssues,
    required String? description,
    required String? wikipediaUrl,
  }) = _CatDto;

  CatEntity toEntity() => CatEntity(
        id: id,
        name: name,
        origin: origin,
        description: description,
        healthIssues: healthIssues,
        intelligence: intelligence,
        wikipediaUrl: wikipediaUrl,
      );

  factory CatDto.fromJson(Map<String, dynamic> json) =>
      _$$CatDtoImplFromJson(json);

  factory CatDto.fromEntity(CatEntity entity) {
    return CatDto(
      id: entity.id,
      name: entity.name,
      origin: entity.origin,
      description: entity.description,
      healthIssues: entity.healthIssues,
      intelligence: entity.intelligence,
      wikipediaUrl: entity.wikipediaUrl,
    );
  }
}

// extension UserModelMapper on CatDto {
//   CatDto toEntitie(CatEntity entity) => CatDto(
//         id: entity.id,
//         name: entity.name,
//         origin: entity.origin,
//         healthIssues: entity.healthIssues,
//         intelligence: entity.intelligence,
//         wikipediaUrl: entity.wikipediaUrl,
//       );
// }
