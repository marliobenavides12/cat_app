import 'package:flutter/material.dart';

import 'package:carousel_slider/carousel_slider.dart';

import '../../../blocs/home/home_bloc.dart';
import 'package:cat_app/presentation/pages/wikipwdia/wikipedia_page.dart';

class CatInformation extends StatelessWidget {
  const CatInformation({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<HomeBloc>().state;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildImages(state),
        const SizedBox(height: 50.0),
        _buildInfo(state: state, context: context),
      ],
    );
  }

  Widget _buildImages(HomeState state) => CarouselSlider.builder(
        itemCount: state.imagesCatList.length,
        itemBuilder: (BuildContext context, int itemIndex, int pageViewIndex) =>
            Container(
          clipBehavior: Clip.antiAlias,
          decoration: BoxDecoration(
            // color: Colors.red,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Image(
            fit: BoxFit.cover,
            image: NetworkImage(state.imagesCatList[itemIndex].url ?? ''),
          ),
        ),
        options: CarouselOptions(
          reverse: false,
          autoPlay: true,
          initialPage: 0,
          enlargeFactor: 0.2,
          aspectRatio: 16 / 9,
          viewportFraction: 0.8,
          enlargeCenterPage: true,
          enableInfiniteScroll: true,
          scrollDirection: Axis.horizontal,
          autoPlayCurve: Curves.fastOutSlowIn,
          autoPlayInterval: const Duration(seconds: 3),
          autoPlayAnimationDuration: const Duration(milliseconds: 800),
        ),
      );

  Widget _buildInfo({
    required HomeState state,
    required BuildContext context,
  }) =>
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  '${state.selectedCat?.name ?? ''} ',
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  '(${state.selectedCat?.origin ?? ''})',
                  style: const TextStyle(fontSize: 16),
                ),
              ],
            ),
            const SizedBox(height: 20.0),
            Text(
              'Vida ${state.selectedCat?.healthIssues ?? ''}',
              style: const TextStyle(fontSize: 16),
            ),
            Text(
              'Inteligencia ${state.selectedCat?.intelligence ?? ''}',
              style: const TextStyle(fontSize: 16),
            ),
            InkWell(
              onTap: () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) =>
                      WikipediaView(url: state.selectedCat?.wikipediaUrl ?? ''),
                ),
              ),
              borderRadius: BorderRadius.circular(6),
              overlayColor: MaterialStateProperty.all(Colors.blue.shade100),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  border: Border.all(color: Colors.blue),
                ),
                padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 8),
                child: const Text(
                  'Wikipedia',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.blue,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}


    /*  CarouselSlider(
          items: [
            // Text('data'),
            // Text('data'),
            // Text('data'),
            Container(
              clipBehavior: Clip.antiAlias,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Image(
                image: NetworkImage(
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiwnut9M00ZbFBShKox5pcqVppLD8I-ZrP-RS9snQvlSYbUE4LbDTlUQcpHQlmabHsl1k&usqp=CAU'),
                fit: BoxFit.fill,
              ),
            ),
            Container(
              clipBehavior: Clip.antiAlias,
              decoration: BoxDecoration(
                // color: Colors.red,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Image(
                image: NetworkImage(
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiwnut9M00ZbFBShKox5pcqVppLD8I-ZrP-RS9snQvlSYbUE4LbDTlUQcpHQlmabHsl1k&usqp=CAU'),
                fit: BoxFit.fill,
              ),
            ),
          ],
          // carouselController: buttonCarouselController,
          // options: CarouselOptions(
          //   autoPlay: true,
          //   initialPage: 2,
          //   aspectRatio: 2.0,
          //   viewportFraction: 0.9,
          //   enlargeCenterPage: true,
          // ),
          options: CarouselOptions(
            reverse: false,
            autoPlay: true,
            initialPage: 0,
            enlargeFactor: 0.3,
            aspectRatio: 16 / 9,
            viewportFraction: 0.8,
            enlargeCenterPage: true,
            enableInfiniteScroll: true,
            scrollDirection: Axis.horizontal,
            autoPlayCurve: Curves.fastOutSlowIn,
            autoPlayInterval: const Duration(seconds: 3),
            autoPlayAnimationDuration: const Duration(milliseconds: 800),
          ),
        ), */
    