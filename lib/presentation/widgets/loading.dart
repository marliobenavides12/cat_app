import 'package:flutter/material.dart';

import 'package:lottie/lottie.dart';
import 'package:animate_do/animate_do.dart';

class Loading extends StatelessWidget {
  const Loading({super.key});

  @override
  Widget build(BuildContext context) => FadeInUpBig(
        child: Lottie.asset(
          'assets/lottie_files/loading_animation.json',
          width: 260,
          fit: BoxFit.contain,
        ),
      );
}
