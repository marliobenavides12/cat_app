import 'package:equatable/equatable.dart';

import 'package:cat_app/domain/entities/cat_entity.dart';
import 'package:cat_app/domain/entities/image_cat_entity.dart';

class CatBreedVoteEntity extends Equatable {
  final CatEntity? cat;
  final ImageCatEntity? imageCat;

  const CatBreedVoteEntity({this.cat, this.imageCat});

  @override
  List<Object?> get props => [cat, imageCat];
}
