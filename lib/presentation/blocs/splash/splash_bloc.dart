import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

export 'package:flutter_bloc/flutter_bloc.dart';

part 'splash_event.dart';
part 'splash_state.dart';
part 'splash_bloc.freezed.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc() : super(SplashState.initial()) {
    on<_StartedEvent>(_goHome);
  }

  Future<void> _goHome(_StartedEvent event, Emitter<SplashState> emit) async {
    await Future.delayed(const Duration(seconds: 4));
    emit(state.copyWith(isLoading: false));
  }
}
