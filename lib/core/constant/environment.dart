import 'package:flutter_dotenv/flutter_dotenv.dart';

class Env {
  Env._();

  static String baseUrl = dotenv.get('BASE_URL');
  static String apiKey = dotenv.get('API_KEY');
}
