// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_cat_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ImageCatDtoImpl _$$ImageCatDtoImplFromJson(Map<String, dynamic> json) =>
    _$ImageCatDtoImpl(
      width: json['width'] as int?,
      id: json['id'] as String?,
      height: json['height'] as int?,
      url: json['url'] as String?,
    );

Map<String, dynamic> _$$ImageCatDtoImplToJson(_$ImageCatDtoImpl instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('width', instance.width);
  writeNotNull('id', instance.id);
  writeNotNull('height', instance.height);
  writeNotNull('url', instance.url);
  return val;
}
