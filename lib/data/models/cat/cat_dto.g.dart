// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cat_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$CatDtoImpl _$$CatDtoImplFromJson(Map<String, dynamic> json) => _$CatDtoImpl(
      id: json['id'] as String?,
      name: json['name'] as String?,
      origin: json['origin'] as String?,
      intelligence: json['intelligence'] as int?,
      healthIssues: json['health_issues'] as int?,
      description: json['description'] as String?,
      wikipediaUrl: json['wikipedia_url'] as String?,
    );

Map<String, dynamic> _$$CatDtoImplToJson(_$CatDtoImpl instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('name', instance.name);
  writeNotNull('origin', instance.origin);
  writeNotNull('intelligence', instance.intelligence);
  writeNotNull('health_issues', instance.healthIssues);
  writeNotNull('description', instance.description);
  writeNotNull('wikipedia_url', instance.wikipediaUrl);
  return val;
}
