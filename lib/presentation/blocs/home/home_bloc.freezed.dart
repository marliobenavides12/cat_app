// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'home_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$HomeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startedEvent,
    required TResult Function() cleanDataEvent,
    required TResult Function(CatEntity? selectedCat) selectedCatEvent,
    required TResult Function(int currentPage) currentPageEvent,
    required TResult Function() getCatsListEvent,
    required TResult Function(String breedId) getImagesCatEvent,
    required TResult Function() getRamdomCatBreedEvent,
    required TResult Function(VoteType voteType) voteForCatBreedEvent,
    required TResult Function() swipeAndLoadEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? startedEvent,
    TResult? Function()? cleanDataEvent,
    TResult? Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult? Function(int currentPage)? currentPageEvent,
    TResult? Function()? getCatsListEvent,
    TResult? Function(String breedId)? getImagesCatEvent,
    TResult? Function()? getRamdomCatBreedEvent,
    TResult? Function(VoteType voteType)? voteForCatBreedEvent,
    TResult? Function()? swipeAndLoadEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startedEvent,
    TResult Function()? cleanDataEvent,
    TResult Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult Function(int currentPage)? currentPageEvent,
    TResult Function()? getCatsListEvent,
    TResult Function(String breedId)? getImagesCatEvent,
    TResult Function()? getRamdomCatBreedEvent,
    TResult Function(VoteType voteType)? voteForCatBreedEvent,
    TResult Function()? swipeAndLoadEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StartedEvent value) startedEvent,
    required TResult Function(_CleanDataEvent value) cleanDataEvent,
    required TResult Function(_SelectedCatEvent value) selectedCatEvent,
    required TResult Function(_CurrentPageEvent value) currentPageEvent,
    required TResult Function(_GetCatListEvent value) getCatsListEvent,
    required TResult Function(_GetImagesCatEvent value) getImagesCatEvent,
    required TResult Function(_GetRamdomCatBreedEvent value)
        getRamdomCatBreedEvent,
    required TResult Function(_VoteForCatBreedEvent value) voteForCatBreedEvent,
    required TResult Function(_SwipeAndLoadEvent value) swipeAndLoadEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StartedEvent value)? startedEvent,
    TResult? Function(_CleanDataEvent value)? cleanDataEvent,
    TResult? Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult? Function(_CurrentPageEvent value)? currentPageEvent,
    TResult? Function(_GetCatListEvent value)? getCatsListEvent,
    TResult? Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult? Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult? Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult? Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StartedEvent value)? startedEvent,
    TResult Function(_CleanDataEvent value)? cleanDataEvent,
    TResult Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult Function(_CurrentPageEvent value)? currentPageEvent,
    TResult Function(_GetCatListEvent value)? getCatsListEvent,
    TResult Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeEventCopyWith<$Res> {
  factory $HomeEventCopyWith(HomeEvent value, $Res Function(HomeEvent) then) =
      _$HomeEventCopyWithImpl<$Res, HomeEvent>;
}

/// @nodoc
class _$HomeEventCopyWithImpl<$Res, $Val extends HomeEvent>
    implements $HomeEventCopyWith<$Res> {
  _$HomeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$StartedEventImplCopyWith<$Res> {
  factory _$$StartedEventImplCopyWith(
          _$StartedEventImpl value, $Res Function(_$StartedEventImpl) then) =
      __$$StartedEventImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StartedEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$StartedEventImpl>
    implements _$$StartedEventImplCopyWith<$Res> {
  __$$StartedEventImplCopyWithImpl(
      _$StartedEventImpl _value, $Res Function(_$StartedEventImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$StartedEventImpl implements _StartedEvent {
  const _$StartedEventImpl();

  @override
  String toString() {
    return 'HomeEvent.startedEvent()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$StartedEventImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startedEvent,
    required TResult Function() cleanDataEvent,
    required TResult Function(CatEntity? selectedCat) selectedCatEvent,
    required TResult Function(int currentPage) currentPageEvent,
    required TResult Function() getCatsListEvent,
    required TResult Function(String breedId) getImagesCatEvent,
    required TResult Function() getRamdomCatBreedEvent,
    required TResult Function(VoteType voteType) voteForCatBreedEvent,
    required TResult Function() swipeAndLoadEvent,
  }) {
    return startedEvent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? startedEvent,
    TResult? Function()? cleanDataEvent,
    TResult? Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult? Function(int currentPage)? currentPageEvent,
    TResult? Function()? getCatsListEvent,
    TResult? Function(String breedId)? getImagesCatEvent,
    TResult? Function()? getRamdomCatBreedEvent,
    TResult? Function(VoteType voteType)? voteForCatBreedEvent,
    TResult? Function()? swipeAndLoadEvent,
  }) {
    return startedEvent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startedEvent,
    TResult Function()? cleanDataEvent,
    TResult Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult Function(int currentPage)? currentPageEvent,
    TResult Function()? getCatsListEvent,
    TResult Function(String breedId)? getImagesCatEvent,
    TResult Function()? getRamdomCatBreedEvent,
    TResult Function(VoteType voteType)? voteForCatBreedEvent,
    TResult Function()? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (startedEvent != null) {
      return startedEvent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StartedEvent value) startedEvent,
    required TResult Function(_CleanDataEvent value) cleanDataEvent,
    required TResult Function(_SelectedCatEvent value) selectedCatEvent,
    required TResult Function(_CurrentPageEvent value) currentPageEvent,
    required TResult Function(_GetCatListEvent value) getCatsListEvent,
    required TResult Function(_GetImagesCatEvent value) getImagesCatEvent,
    required TResult Function(_GetRamdomCatBreedEvent value)
        getRamdomCatBreedEvent,
    required TResult Function(_VoteForCatBreedEvent value) voteForCatBreedEvent,
    required TResult Function(_SwipeAndLoadEvent value) swipeAndLoadEvent,
  }) {
    return startedEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StartedEvent value)? startedEvent,
    TResult? Function(_CleanDataEvent value)? cleanDataEvent,
    TResult? Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult? Function(_CurrentPageEvent value)? currentPageEvent,
    TResult? Function(_GetCatListEvent value)? getCatsListEvent,
    TResult? Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult? Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult? Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult? Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
  }) {
    return startedEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StartedEvent value)? startedEvent,
    TResult Function(_CleanDataEvent value)? cleanDataEvent,
    TResult Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult Function(_CurrentPageEvent value)? currentPageEvent,
    TResult Function(_GetCatListEvent value)? getCatsListEvent,
    TResult Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (startedEvent != null) {
      return startedEvent(this);
    }
    return orElse();
  }
}

abstract class _StartedEvent implements HomeEvent {
  const factory _StartedEvent() = _$StartedEventImpl;
}

/// @nodoc
abstract class _$$CleanDataEventImplCopyWith<$Res> {
  factory _$$CleanDataEventImplCopyWith(_$CleanDataEventImpl value,
          $Res Function(_$CleanDataEventImpl) then) =
      __$$CleanDataEventImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CleanDataEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$CleanDataEventImpl>
    implements _$$CleanDataEventImplCopyWith<$Res> {
  __$$CleanDataEventImplCopyWithImpl(
      _$CleanDataEventImpl _value, $Res Function(_$CleanDataEventImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CleanDataEventImpl implements _CleanDataEvent {
  const _$CleanDataEventImpl();

  @override
  String toString() {
    return 'HomeEvent.cleanDataEvent()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CleanDataEventImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startedEvent,
    required TResult Function() cleanDataEvent,
    required TResult Function(CatEntity? selectedCat) selectedCatEvent,
    required TResult Function(int currentPage) currentPageEvent,
    required TResult Function() getCatsListEvent,
    required TResult Function(String breedId) getImagesCatEvent,
    required TResult Function() getRamdomCatBreedEvent,
    required TResult Function(VoteType voteType) voteForCatBreedEvent,
    required TResult Function() swipeAndLoadEvent,
  }) {
    return cleanDataEvent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? startedEvent,
    TResult? Function()? cleanDataEvent,
    TResult? Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult? Function(int currentPage)? currentPageEvent,
    TResult? Function()? getCatsListEvent,
    TResult? Function(String breedId)? getImagesCatEvent,
    TResult? Function()? getRamdomCatBreedEvent,
    TResult? Function(VoteType voteType)? voteForCatBreedEvent,
    TResult? Function()? swipeAndLoadEvent,
  }) {
    return cleanDataEvent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startedEvent,
    TResult Function()? cleanDataEvent,
    TResult Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult Function(int currentPage)? currentPageEvent,
    TResult Function()? getCatsListEvent,
    TResult Function(String breedId)? getImagesCatEvent,
    TResult Function()? getRamdomCatBreedEvent,
    TResult Function(VoteType voteType)? voteForCatBreedEvent,
    TResult Function()? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (cleanDataEvent != null) {
      return cleanDataEvent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StartedEvent value) startedEvent,
    required TResult Function(_CleanDataEvent value) cleanDataEvent,
    required TResult Function(_SelectedCatEvent value) selectedCatEvent,
    required TResult Function(_CurrentPageEvent value) currentPageEvent,
    required TResult Function(_GetCatListEvent value) getCatsListEvent,
    required TResult Function(_GetImagesCatEvent value) getImagesCatEvent,
    required TResult Function(_GetRamdomCatBreedEvent value)
        getRamdomCatBreedEvent,
    required TResult Function(_VoteForCatBreedEvent value) voteForCatBreedEvent,
    required TResult Function(_SwipeAndLoadEvent value) swipeAndLoadEvent,
  }) {
    return cleanDataEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StartedEvent value)? startedEvent,
    TResult? Function(_CleanDataEvent value)? cleanDataEvent,
    TResult? Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult? Function(_CurrentPageEvent value)? currentPageEvent,
    TResult? Function(_GetCatListEvent value)? getCatsListEvent,
    TResult? Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult? Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult? Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult? Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
  }) {
    return cleanDataEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StartedEvent value)? startedEvent,
    TResult Function(_CleanDataEvent value)? cleanDataEvent,
    TResult Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult Function(_CurrentPageEvent value)? currentPageEvent,
    TResult Function(_GetCatListEvent value)? getCatsListEvent,
    TResult Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (cleanDataEvent != null) {
      return cleanDataEvent(this);
    }
    return orElse();
  }
}

abstract class _CleanDataEvent implements HomeEvent {
  const factory _CleanDataEvent() = _$CleanDataEventImpl;
}

/// @nodoc
abstract class _$$SelectedCatEventImplCopyWith<$Res> {
  factory _$$SelectedCatEventImplCopyWith(_$SelectedCatEventImpl value,
          $Res Function(_$SelectedCatEventImpl) then) =
      __$$SelectedCatEventImplCopyWithImpl<$Res>;
  @useResult
  $Res call({CatEntity? selectedCat});
}

/// @nodoc
class __$$SelectedCatEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$SelectedCatEventImpl>
    implements _$$SelectedCatEventImplCopyWith<$Res> {
  __$$SelectedCatEventImplCopyWithImpl(_$SelectedCatEventImpl _value,
      $Res Function(_$SelectedCatEventImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedCat = freezed,
  }) {
    return _then(_$SelectedCatEventImpl(
      freezed == selectedCat
          ? _value.selectedCat
          : selectedCat // ignore: cast_nullable_to_non_nullable
              as CatEntity?,
    ));
  }
}

/// @nodoc

class _$SelectedCatEventImpl implements _SelectedCatEvent {
  const _$SelectedCatEventImpl(this.selectedCat);

  @override
  final CatEntity? selectedCat;

  @override
  String toString() {
    return 'HomeEvent.selectedCatEvent(selectedCat: $selectedCat)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SelectedCatEventImpl &&
            (identical(other.selectedCat, selectedCat) ||
                other.selectedCat == selectedCat));
  }

  @override
  int get hashCode => Object.hash(runtimeType, selectedCat);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SelectedCatEventImplCopyWith<_$SelectedCatEventImpl> get copyWith =>
      __$$SelectedCatEventImplCopyWithImpl<_$SelectedCatEventImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startedEvent,
    required TResult Function() cleanDataEvent,
    required TResult Function(CatEntity? selectedCat) selectedCatEvent,
    required TResult Function(int currentPage) currentPageEvent,
    required TResult Function() getCatsListEvent,
    required TResult Function(String breedId) getImagesCatEvent,
    required TResult Function() getRamdomCatBreedEvent,
    required TResult Function(VoteType voteType) voteForCatBreedEvent,
    required TResult Function() swipeAndLoadEvent,
  }) {
    return selectedCatEvent(selectedCat);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? startedEvent,
    TResult? Function()? cleanDataEvent,
    TResult? Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult? Function(int currentPage)? currentPageEvent,
    TResult? Function()? getCatsListEvent,
    TResult? Function(String breedId)? getImagesCatEvent,
    TResult? Function()? getRamdomCatBreedEvent,
    TResult? Function(VoteType voteType)? voteForCatBreedEvent,
    TResult? Function()? swipeAndLoadEvent,
  }) {
    return selectedCatEvent?.call(selectedCat);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startedEvent,
    TResult Function()? cleanDataEvent,
    TResult Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult Function(int currentPage)? currentPageEvent,
    TResult Function()? getCatsListEvent,
    TResult Function(String breedId)? getImagesCatEvent,
    TResult Function()? getRamdomCatBreedEvent,
    TResult Function(VoteType voteType)? voteForCatBreedEvent,
    TResult Function()? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (selectedCatEvent != null) {
      return selectedCatEvent(selectedCat);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StartedEvent value) startedEvent,
    required TResult Function(_CleanDataEvent value) cleanDataEvent,
    required TResult Function(_SelectedCatEvent value) selectedCatEvent,
    required TResult Function(_CurrentPageEvent value) currentPageEvent,
    required TResult Function(_GetCatListEvent value) getCatsListEvent,
    required TResult Function(_GetImagesCatEvent value) getImagesCatEvent,
    required TResult Function(_GetRamdomCatBreedEvent value)
        getRamdomCatBreedEvent,
    required TResult Function(_VoteForCatBreedEvent value) voteForCatBreedEvent,
    required TResult Function(_SwipeAndLoadEvent value) swipeAndLoadEvent,
  }) {
    return selectedCatEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StartedEvent value)? startedEvent,
    TResult? Function(_CleanDataEvent value)? cleanDataEvent,
    TResult? Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult? Function(_CurrentPageEvent value)? currentPageEvent,
    TResult? Function(_GetCatListEvent value)? getCatsListEvent,
    TResult? Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult? Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult? Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult? Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
  }) {
    return selectedCatEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StartedEvent value)? startedEvent,
    TResult Function(_CleanDataEvent value)? cleanDataEvent,
    TResult Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult Function(_CurrentPageEvent value)? currentPageEvent,
    TResult Function(_GetCatListEvent value)? getCatsListEvent,
    TResult Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (selectedCatEvent != null) {
      return selectedCatEvent(this);
    }
    return orElse();
  }
}

abstract class _SelectedCatEvent implements HomeEvent {
  const factory _SelectedCatEvent(final CatEntity? selectedCat) =
      _$SelectedCatEventImpl;

  CatEntity? get selectedCat;
  @JsonKey(ignore: true)
  _$$SelectedCatEventImplCopyWith<_$SelectedCatEventImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CurrentPageEventImplCopyWith<$Res> {
  factory _$$CurrentPageEventImplCopyWith(_$CurrentPageEventImpl value,
          $Res Function(_$CurrentPageEventImpl) then) =
      __$$CurrentPageEventImplCopyWithImpl<$Res>;
  @useResult
  $Res call({int currentPage});
}

/// @nodoc
class __$$CurrentPageEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$CurrentPageEventImpl>
    implements _$$CurrentPageEventImplCopyWith<$Res> {
  __$$CurrentPageEventImplCopyWithImpl(_$CurrentPageEventImpl _value,
      $Res Function(_$CurrentPageEventImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? currentPage = null,
  }) {
    return _then(_$CurrentPageEventImpl(
      null == currentPage
          ? _value.currentPage
          : currentPage // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$CurrentPageEventImpl implements _CurrentPageEvent {
  const _$CurrentPageEventImpl(this.currentPage);

  @override
  final int currentPage;

  @override
  String toString() {
    return 'HomeEvent.currentPageEvent(currentPage: $currentPage)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CurrentPageEventImpl &&
            (identical(other.currentPage, currentPage) ||
                other.currentPage == currentPage));
  }

  @override
  int get hashCode => Object.hash(runtimeType, currentPage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CurrentPageEventImplCopyWith<_$CurrentPageEventImpl> get copyWith =>
      __$$CurrentPageEventImplCopyWithImpl<_$CurrentPageEventImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startedEvent,
    required TResult Function() cleanDataEvent,
    required TResult Function(CatEntity? selectedCat) selectedCatEvent,
    required TResult Function(int currentPage) currentPageEvent,
    required TResult Function() getCatsListEvent,
    required TResult Function(String breedId) getImagesCatEvent,
    required TResult Function() getRamdomCatBreedEvent,
    required TResult Function(VoteType voteType) voteForCatBreedEvent,
    required TResult Function() swipeAndLoadEvent,
  }) {
    return currentPageEvent(currentPage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? startedEvent,
    TResult? Function()? cleanDataEvent,
    TResult? Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult? Function(int currentPage)? currentPageEvent,
    TResult? Function()? getCatsListEvent,
    TResult? Function(String breedId)? getImagesCatEvent,
    TResult? Function()? getRamdomCatBreedEvent,
    TResult? Function(VoteType voteType)? voteForCatBreedEvent,
    TResult? Function()? swipeAndLoadEvent,
  }) {
    return currentPageEvent?.call(currentPage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startedEvent,
    TResult Function()? cleanDataEvent,
    TResult Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult Function(int currentPage)? currentPageEvent,
    TResult Function()? getCatsListEvent,
    TResult Function(String breedId)? getImagesCatEvent,
    TResult Function()? getRamdomCatBreedEvent,
    TResult Function(VoteType voteType)? voteForCatBreedEvent,
    TResult Function()? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (currentPageEvent != null) {
      return currentPageEvent(currentPage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StartedEvent value) startedEvent,
    required TResult Function(_CleanDataEvent value) cleanDataEvent,
    required TResult Function(_SelectedCatEvent value) selectedCatEvent,
    required TResult Function(_CurrentPageEvent value) currentPageEvent,
    required TResult Function(_GetCatListEvent value) getCatsListEvent,
    required TResult Function(_GetImagesCatEvent value) getImagesCatEvent,
    required TResult Function(_GetRamdomCatBreedEvent value)
        getRamdomCatBreedEvent,
    required TResult Function(_VoteForCatBreedEvent value) voteForCatBreedEvent,
    required TResult Function(_SwipeAndLoadEvent value) swipeAndLoadEvent,
  }) {
    return currentPageEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StartedEvent value)? startedEvent,
    TResult? Function(_CleanDataEvent value)? cleanDataEvent,
    TResult? Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult? Function(_CurrentPageEvent value)? currentPageEvent,
    TResult? Function(_GetCatListEvent value)? getCatsListEvent,
    TResult? Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult? Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult? Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult? Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
  }) {
    return currentPageEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StartedEvent value)? startedEvent,
    TResult Function(_CleanDataEvent value)? cleanDataEvent,
    TResult Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult Function(_CurrentPageEvent value)? currentPageEvent,
    TResult Function(_GetCatListEvent value)? getCatsListEvent,
    TResult Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (currentPageEvent != null) {
      return currentPageEvent(this);
    }
    return orElse();
  }
}

abstract class _CurrentPageEvent implements HomeEvent {
  const factory _CurrentPageEvent(final int currentPage) =
      _$CurrentPageEventImpl;

  int get currentPage;
  @JsonKey(ignore: true)
  _$$CurrentPageEventImplCopyWith<_$CurrentPageEventImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetCatListEventImplCopyWith<$Res> {
  factory _$$GetCatListEventImplCopyWith(_$GetCatListEventImpl value,
          $Res Function(_$GetCatListEventImpl) then) =
      __$$GetCatListEventImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetCatListEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$GetCatListEventImpl>
    implements _$$GetCatListEventImplCopyWith<$Res> {
  __$$GetCatListEventImplCopyWithImpl(
      _$GetCatListEventImpl _value, $Res Function(_$GetCatListEventImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetCatListEventImpl implements _GetCatListEvent {
  const _$GetCatListEventImpl();

  @override
  String toString() {
    return 'HomeEvent.getCatsListEvent()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetCatListEventImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startedEvent,
    required TResult Function() cleanDataEvent,
    required TResult Function(CatEntity? selectedCat) selectedCatEvent,
    required TResult Function(int currentPage) currentPageEvent,
    required TResult Function() getCatsListEvent,
    required TResult Function(String breedId) getImagesCatEvent,
    required TResult Function() getRamdomCatBreedEvent,
    required TResult Function(VoteType voteType) voteForCatBreedEvent,
    required TResult Function() swipeAndLoadEvent,
  }) {
    return getCatsListEvent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? startedEvent,
    TResult? Function()? cleanDataEvent,
    TResult? Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult? Function(int currentPage)? currentPageEvent,
    TResult? Function()? getCatsListEvent,
    TResult? Function(String breedId)? getImagesCatEvent,
    TResult? Function()? getRamdomCatBreedEvent,
    TResult? Function(VoteType voteType)? voteForCatBreedEvent,
    TResult? Function()? swipeAndLoadEvent,
  }) {
    return getCatsListEvent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startedEvent,
    TResult Function()? cleanDataEvent,
    TResult Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult Function(int currentPage)? currentPageEvent,
    TResult Function()? getCatsListEvent,
    TResult Function(String breedId)? getImagesCatEvent,
    TResult Function()? getRamdomCatBreedEvent,
    TResult Function(VoteType voteType)? voteForCatBreedEvent,
    TResult Function()? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (getCatsListEvent != null) {
      return getCatsListEvent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StartedEvent value) startedEvent,
    required TResult Function(_CleanDataEvent value) cleanDataEvent,
    required TResult Function(_SelectedCatEvent value) selectedCatEvent,
    required TResult Function(_CurrentPageEvent value) currentPageEvent,
    required TResult Function(_GetCatListEvent value) getCatsListEvent,
    required TResult Function(_GetImagesCatEvent value) getImagesCatEvent,
    required TResult Function(_GetRamdomCatBreedEvent value)
        getRamdomCatBreedEvent,
    required TResult Function(_VoteForCatBreedEvent value) voteForCatBreedEvent,
    required TResult Function(_SwipeAndLoadEvent value) swipeAndLoadEvent,
  }) {
    return getCatsListEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StartedEvent value)? startedEvent,
    TResult? Function(_CleanDataEvent value)? cleanDataEvent,
    TResult? Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult? Function(_CurrentPageEvent value)? currentPageEvent,
    TResult? Function(_GetCatListEvent value)? getCatsListEvent,
    TResult? Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult? Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult? Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult? Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
  }) {
    return getCatsListEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StartedEvent value)? startedEvent,
    TResult Function(_CleanDataEvent value)? cleanDataEvent,
    TResult Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult Function(_CurrentPageEvent value)? currentPageEvent,
    TResult Function(_GetCatListEvent value)? getCatsListEvent,
    TResult Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (getCatsListEvent != null) {
      return getCatsListEvent(this);
    }
    return orElse();
  }
}

abstract class _GetCatListEvent implements HomeEvent {
  const factory _GetCatListEvent() = _$GetCatListEventImpl;
}

/// @nodoc
abstract class _$$GetImagesCatEventImplCopyWith<$Res> {
  factory _$$GetImagesCatEventImplCopyWith(_$GetImagesCatEventImpl value,
          $Res Function(_$GetImagesCatEventImpl) then) =
      __$$GetImagesCatEventImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String breedId});
}

/// @nodoc
class __$$GetImagesCatEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$GetImagesCatEventImpl>
    implements _$$GetImagesCatEventImplCopyWith<$Res> {
  __$$GetImagesCatEventImplCopyWithImpl(_$GetImagesCatEventImpl _value,
      $Res Function(_$GetImagesCatEventImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? breedId = null,
  }) {
    return _then(_$GetImagesCatEventImpl(
      null == breedId
          ? _value.breedId
          : breedId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$GetImagesCatEventImpl implements _GetImagesCatEvent {
  const _$GetImagesCatEventImpl(this.breedId);

  @override
  final String breedId;

  @override
  String toString() {
    return 'HomeEvent.getImagesCatEvent(breedId: $breedId)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetImagesCatEventImpl &&
            (identical(other.breedId, breedId) || other.breedId == breedId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, breedId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GetImagesCatEventImplCopyWith<_$GetImagesCatEventImpl> get copyWith =>
      __$$GetImagesCatEventImplCopyWithImpl<_$GetImagesCatEventImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startedEvent,
    required TResult Function() cleanDataEvent,
    required TResult Function(CatEntity? selectedCat) selectedCatEvent,
    required TResult Function(int currentPage) currentPageEvent,
    required TResult Function() getCatsListEvent,
    required TResult Function(String breedId) getImagesCatEvent,
    required TResult Function() getRamdomCatBreedEvent,
    required TResult Function(VoteType voteType) voteForCatBreedEvent,
    required TResult Function() swipeAndLoadEvent,
  }) {
    return getImagesCatEvent(breedId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? startedEvent,
    TResult? Function()? cleanDataEvent,
    TResult? Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult? Function(int currentPage)? currentPageEvent,
    TResult? Function()? getCatsListEvent,
    TResult? Function(String breedId)? getImagesCatEvent,
    TResult? Function()? getRamdomCatBreedEvent,
    TResult? Function(VoteType voteType)? voteForCatBreedEvent,
    TResult? Function()? swipeAndLoadEvent,
  }) {
    return getImagesCatEvent?.call(breedId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startedEvent,
    TResult Function()? cleanDataEvent,
    TResult Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult Function(int currentPage)? currentPageEvent,
    TResult Function()? getCatsListEvent,
    TResult Function(String breedId)? getImagesCatEvent,
    TResult Function()? getRamdomCatBreedEvent,
    TResult Function(VoteType voteType)? voteForCatBreedEvent,
    TResult Function()? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (getImagesCatEvent != null) {
      return getImagesCatEvent(breedId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StartedEvent value) startedEvent,
    required TResult Function(_CleanDataEvent value) cleanDataEvent,
    required TResult Function(_SelectedCatEvent value) selectedCatEvent,
    required TResult Function(_CurrentPageEvent value) currentPageEvent,
    required TResult Function(_GetCatListEvent value) getCatsListEvent,
    required TResult Function(_GetImagesCatEvent value) getImagesCatEvent,
    required TResult Function(_GetRamdomCatBreedEvent value)
        getRamdomCatBreedEvent,
    required TResult Function(_VoteForCatBreedEvent value) voteForCatBreedEvent,
    required TResult Function(_SwipeAndLoadEvent value) swipeAndLoadEvent,
  }) {
    return getImagesCatEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StartedEvent value)? startedEvent,
    TResult? Function(_CleanDataEvent value)? cleanDataEvent,
    TResult? Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult? Function(_CurrentPageEvent value)? currentPageEvent,
    TResult? Function(_GetCatListEvent value)? getCatsListEvent,
    TResult? Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult? Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult? Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult? Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
  }) {
    return getImagesCatEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StartedEvent value)? startedEvent,
    TResult Function(_CleanDataEvent value)? cleanDataEvent,
    TResult Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult Function(_CurrentPageEvent value)? currentPageEvent,
    TResult Function(_GetCatListEvent value)? getCatsListEvent,
    TResult Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (getImagesCatEvent != null) {
      return getImagesCatEvent(this);
    }
    return orElse();
  }
}

abstract class _GetImagesCatEvent implements HomeEvent {
  const factory _GetImagesCatEvent(final String breedId) =
      _$GetImagesCatEventImpl;

  String get breedId;
  @JsonKey(ignore: true)
  _$$GetImagesCatEventImplCopyWith<_$GetImagesCatEventImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetRamdomCatBreedEventImplCopyWith<$Res> {
  factory _$$GetRamdomCatBreedEventImplCopyWith(
          _$GetRamdomCatBreedEventImpl value,
          $Res Function(_$GetRamdomCatBreedEventImpl) then) =
      __$$GetRamdomCatBreedEventImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetRamdomCatBreedEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$GetRamdomCatBreedEventImpl>
    implements _$$GetRamdomCatBreedEventImplCopyWith<$Res> {
  __$$GetRamdomCatBreedEventImplCopyWithImpl(
      _$GetRamdomCatBreedEventImpl _value,
      $Res Function(_$GetRamdomCatBreedEventImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetRamdomCatBreedEventImpl implements _GetRamdomCatBreedEvent {
  const _$GetRamdomCatBreedEventImpl();

  @override
  String toString() {
    return 'HomeEvent.getRamdomCatBreedEvent()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetRamdomCatBreedEventImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startedEvent,
    required TResult Function() cleanDataEvent,
    required TResult Function(CatEntity? selectedCat) selectedCatEvent,
    required TResult Function(int currentPage) currentPageEvent,
    required TResult Function() getCatsListEvent,
    required TResult Function(String breedId) getImagesCatEvent,
    required TResult Function() getRamdomCatBreedEvent,
    required TResult Function(VoteType voteType) voteForCatBreedEvent,
    required TResult Function() swipeAndLoadEvent,
  }) {
    return getRamdomCatBreedEvent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? startedEvent,
    TResult? Function()? cleanDataEvent,
    TResult? Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult? Function(int currentPage)? currentPageEvent,
    TResult? Function()? getCatsListEvent,
    TResult? Function(String breedId)? getImagesCatEvent,
    TResult? Function()? getRamdomCatBreedEvent,
    TResult? Function(VoteType voteType)? voteForCatBreedEvent,
    TResult? Function()? swipeAndLoadEvent,
  }) {
    return getRamdomCatBreedEvent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startedEvent,
    TResult Function()? cleanDataEvent,
    TResult Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult Function(int currentPage)? currentPageEvent,
    TResult Function()? getCatsListEvent,
    TResult Function(String breedId)? getImagesCatEvent,
    TResult Function()? getRamdomCatBreedEvent,
    TResult Function(VoteType voteType)? voteForCatBreedEvent,
    TResult Function()? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (getRamdomCatBreedEvent != null) {
      return getRamdomCatBreedEvent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StartedEvent value) startedEvent,
    required TResult Function(_CleanDataEvent value) cleanDataEvent,
    required TResult Function(_SelectedCatEvent value) selectedCatEvent,
    required TResult Function(_CurrentPageEvent value) currentPageEvent,
    required TResult Function(_GetCatListEvent value) getCatsListEvent,
    required TResult Function(_GetImagesCatEvent value) getImagesCatEvent,
    required TResult Function(_GetRamdomCatBreedEvent value)
        getRamdomCatBreedEvent,
    required TResult Function(_VoteForCatBreedEvent value) voteForCatBreedEvent,
    required TResult Function(_SwipeAndLoadEvent value) swipeAndLoadEvent,
  }) {
    return getRamdomCatBreedEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StartedEvent value)? startedEvent,
    TResult? Function(_CleanDataEvent value)? cleanDataEvent,
    TResult? Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult? Function(_CurrentPageEvent value)? currentPageEvent,
    TResult? Function(_GetCatListEvent value)? getCatsListEvent,
    TResult? Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult? Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult? Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult? Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
  }) {
    return getRamdomCatBreedEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StartedEvent value)? startedEvent,
    TResult Function(_CleanDataEvent value)? cleanDataEvent,
    TResult Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult Function(_CurrentPageEvent value)? currentPageEvent,
    TResult Function(_GetCatListEvent value)? getCatsListEvent,
    TResult Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (getRamdomCatBreedEvent != null) {
      return getRamdomCatBreedEvent(this);
    }
    return orElse();
  }
}

abstract class _GetRamdomCatBreedEvent implements HomeEvent {
  const factory _GetRamdomCatBreedEvent() = _$GetRamdomCatBreedEventImpl;
}

/// @nodoc
abstract class _$$VoteForCatBreedEventImplCopyWith<$Res> {
  factory _$$VoteForCatBreedEventImplCopyWith(_$VoteForCatBreedEventImpl value,
          $Res Function(_$VoteForCatBreedEventImpl) then) =
      __$$VoteForCatBreedEventImplCopyWithImpl<$Res>;
  @useResult
  $Res call({VoteType voteType});
}

/// @nodoc
class __$$VoteForCatBreedEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$VoteForCatBreedEventImpl>
    implements _$$VoteForCatBreedEventImplCopyWith<$Res> {
  __$$VoteForCatBreedEventImplCopyWithImpl(_$VoteForCatBreedEventImpl _value,
      $Res Function(_$VoteForCatBreedEventImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? voteType = null,
  }) {
    return _then(_$VoteForCatBreedEventImpl(
      null == voteType
          ? _value.voteType
          : voteType // ignore: cast_nullable_to_non_nullable
              as VoteType,
    ));
  }
}

/// @nodoc

class _$VoteForCatBreedEventImpl implements _VoteForCatBreedEvent {
  const _$VoteForCatBreedEventImpl(this.voteType);

  @override
  final VoteType voteType;

  @override
  String toString() {
    return 'HomeEvent.voteForCatBreedEvent(voteType: $voteType)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$VoteForCatBreedEventImpl &&
            (identical(other.voteType, voteType) ||
                other.voteType == voteType));
  }

  @override
  int get hashCode => Object.hash(runtimeType, voteType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$VoteForCatBreedEventImplCopyWith<_$VoteForCatBreedEventImpl>
      get copyWith =>
          __$$VoteForCatBreedEventImplCopyWithImpl<_$VoteForCatBreedEventImpl>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startedEvent,
    required TResult Function() cleanDataEvent,
    required TResult Function(CatEntity? selectedCat) selectedCatEvent,
    required TResult Function(int currentPage) currentPageEvent,
    required TResult Function() getCatsListEvent,
    required TResult Function(String breedId) getImagesCatEvent,
    required TResult Function() getRamdomCatBreedEvent,
    required TResult Function(VoteType voteType) voteForCatBreedEvent,
    required TResult Function() swipeAndLoadEvent,
  }) {
    return voteForCatBreedEvent(voteType);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? startedEvent,
    TResult? Function()? cleanDataEvent,
    TResult? Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult? Function(int currentPage)? currentPageEvent,
    TResult? Function()? getCatsListEvent,
    TResult? Function(String breedId)? getImagesCatEvent,
    TResult? Function()? getRamdomCatBreedEvent,
    TResult? Function(VoteType voteType)? voteForCatBreedEvent,
    TResult? Function()? swipeAndLoadEvent,
  }) {
    return voteForCatBreedEvent?.call(voteType);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startedEvent,
    TResult Function()? cleanDataEvent,
    TResult Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult Function(int currentPage)? currentPageEvent,
    TResult Function()? getCatsListEvent,
    TResult Function(String breedId)? getImagesCatEvent,
    TResult Function()? getRamdomCatBreedEvent,
    TResult Function(VoteType voteType)? voteForCatBreedEvent,
    TResult Function()? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (voteForCatBreedEvent != null) {
      return voteForCatBreedEvent(voteType);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StartedEvent value) startedEvent,
    required TResult Function(_CleanDataEvent value) cleanDataEvent,
    required TResult Function(_SelectedCatEvent value) selectedCatEvent,
    required TResult Function(_CurrentPageEvent value) currentPageEvent,
    required TResult Function(_GetCatListEvent value) getCatsListEvent,
    required TResult Function(_GetImagesCatEvent value) getImagesCatEvent,
    required TResult Function(_GetRamdomCatBreedEvent value)
        getRamdomCatBreedEvent,
    required TResult Function(_VoteForCatBreedEvent value) voteForCatBreedEvent,
    required TResult Function(_SwipeAndLoadEvent value) swipeAndLoadEvent,
  }) {
    return voteForCatBreedEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StartedEvent value)? startedEvent,
    TResult? Function(_CleanDataEvent value)? cleanDataEvent,
    TResult? Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult? Function(_CurrentPageEvent value)? currentPageEvent,
    TResult? Function(_GetCatListEvent value)? getCatsListEvent,
    TResult? Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult? Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult? Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult? Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
  }) {
    return voteForCatBreedEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StartedEvent value)? startedEvent,
    TResult Function(_CleanDataEvent value)? cleanDataEvent,
    TResult Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult Function(_CurrentPageEvent value)? currentPageEvent,
    TResult Function(_GetCatListEvent value)? getCatsListEvent,
    TResult Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (voteForCatBreedEvent != null) {
      return voteForCatBreedEvent(this);
    }
    return orElse();
  }
}

abstract class _VoteForCatBreedEvent implements HomeEvent {
  const factory _VoteForCatBreedEvent(final VoteType voteType) =
      _$VoteForCatBreedEventImpl;

  VoteType get voteType;
  @JsonKey(ignore: true)
  _$$VoteForCatBreedEventImplCopyWith<_$VoteForCatBreedEventImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SwipeAndLoadEventImplCopyWith<$Res> {
  factory _$$SwipeAndLoadEventImplCopyWith(_$SwipeAndLoadEventImpl value,
          $Res Function(_$SwipeAndLoadEventImpl) then) =
      __$$SwipeAndLoadEventImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SwipeAndLoadEventImplCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$SwipeAndLoadEventImpl>
    implements _$$SwipeAndLoadEventImplCopyWith<$Res> {
  __$$SwipeAndLoadEventImplCopyWithImpl(_$SwipeAndLoadEventImpl _value,
      $Res Function(_$SwipeAndLoadEventImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$SwipeAndLoadEventImpl implements _SwipeAndLoadEvent {
  const _$SwipeAndLoadEventImpl();

  @override
  String toString() {
    return 'HomeEvent.swipeAndLoadEvent()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SwipeAndLoadEventImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startedEvent,
    required TResult Function() cleanDataEvent,
    required TResult Function(CatEntity? selectedCat) selectedCatEvent,
    required TResult Function(int currentPage) currentPageEvent,
    required TResult Function() getCatsListEvent,
    required TResult Function(String breedId) getImagesCatEvent,
    required TResult Function() getRamdomCatBreedEvent,
    required TResult Function(VoteType voteType) voteForCatBreedEvent,
    required TResult Function() swipeAndLoadEvent,
  }) {
    return swipeAndLoadEvent();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? startedEvent,
    TResult? Function()? cleanDataEvent,
    TResult? Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult? Function(int currentPage)? currentPageEvent,
    TResult? Function()? getCatsListEvent,
    TResult? Function(String breedId)? getImagesCatEvent,
    TResult? Function()? getRamdomCatBreedEvent,
    TResult? Function(VoteType voteType)? voteForCatBreedEvent,
    TResult? Function()? swipeAndLoadEvent,
  }) {
    return swipeAndLoadEvent?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startedEvent,
    TResult Function()? cleanDataEvent,
    TResult Function(CatEntity? selectedCat)? selectedCatEvent,
    TResult Function(int currentPage)? currentPageEvent,
    TResult Function()? getCatsListEvent,
    TResult Function(String breedId)? getImagesCatEvent,
    TResult Function()? getRamdomCatBreedEvent,
    TResult Function(VoteType voteType)? voteForCatBreedEvent,
    TResult Function()? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (swipeAndLoadEvent != null) {
      return swipeAndLoadEvent();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_StartedEvent value) startedEvent,
    required TResult Function(_CleanDataEvent value) cleanDataEvent,
    required TResult Function(_SelectedCatEvent value) selectedCatEvent,
    required TResult Function(_CurrentPageEvent value) currentPageEvent,
    required TResult Function(_GetCatListEvent value) getCatsListEvent,
    required TResult Function(_GetImagesCatEvent value) getImagesCatEvent,
    required TResult Function(_GetRamdomCatBreedEvent value)
        getRamdomCatBreedEvent,
    required TResult Function(_VoteForCatBreedEvent value) voteForCatBreedEvent,
    required TResult Function(_SwipeAndLoadEvent value) swipeAndLoadEvent,
  }) {
    return swipeAndLoadEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_StartedEvent value)? startedEvent,
    TResult? Function(_CleanDataEvent value)? cleanDataEvent,
    TResult? Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult? Function(_CurrentPageEvent value)? currentPageEvent,
    TResult? Function(_GetCatListEvent value)? getCatsListEvent,
    TResult? Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult? Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult? Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult? Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
  }) {
    return swipeAndLoadEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_StartedEvent value)? startedEvent,
    TResult Function(_CleanDataEvent value)? cleanDataEvent,
    TResult Function(_SelectedCatEvent value)? selectedCatEvent,
    TResult Function(_CurrentPageEvent value)? currentPageEvent,
    TResult Function(_GetCatListEvent value)? getCatsListEvent,
    TResult Function(_GetImagesCatEvent value)? getImagesCatEvent,
    TResult Function(_GetRamdomCatBreedEvent value)? getRamdomCatBreedEvent,
    TResult Function(_VoteForCatBreedEvent value)? voteForCatBreedEvent,
    TResult Function(_SwipeAndLoadEvent value)? swipeAndLoadEvent,
    required TResult orElse(),
  }) {
    if (swipeAndLoadEvent != null) {
      return swipeAndLoadEvent(this);
    }
    return orElse();
  }
}

abstract class _SwipeAndLoadEvent implements HomeEvent {
  const factory _SwipeAndLoadEvent() = _$SwipeAndLoadEventImpl;
}

/// @nodoc
mixin _$HomeState {
  bool get isLoading => throw _privateConstructorUsedError;
  int get currentPage => throw _privateConstructorUsedError;
  bool get isLoadingVotePage => throw _privateConstructorUsedError;
  CatEntity? get selectedCat => throw _privateConstructorUsedError;
  List<CatEntity> get catList => throw _privateConstructorUsedError;
  bool get isLoadingSearchPage => throw _privateConstructorUsedError;
  PageController get pageController => throw _privateConstructorUsedError;
  CatBreedVoteEntity? get catBreedVote => throw _privateConstructorUsedError;
  List<ImageCatEntity> get imagesCatList => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $HomeStateCopyWith<HomeState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeStateCopyWith<$Res> {
  factory $HomeStateCopyWith(HomeState value, $Res Function(HomeState) then) =
      _$HomeStateCopyWithImpl<$Res, HomeState>;
  @useResult
  $Res call(
      {bool isLoading,
      int currentPage,
      bool isLoadingVotePage,
      CatEntity? selectedCat,
      List<CatEntity> catList,
      bool isLoadingSearchPage,
      PageController pageController,
      CatBreedVoteEntity? catBreedVote,
      List<ImageCatEntity> imagesCatList});
}

/// @nodoc
class _$HomeStateCopyWithImpl<$Res, $Val extends HomeState>
    implements $HomeStateCopyWith<$Res> {
  _$HomeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? currentPage = null,
    Object? isLoadingVotePage = null,
    Object? selectedCat = freezed,
    Object? catList = null,
    Object? isLoadingSearchPage = null,
    Object? pageController = null,
    Object? catBreedVote = freezed,
    Object? imagesCatList = null,
  }) {
    return _then(_value.copyWith(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      currentPage: null == currentPage
          ? _value.currentPage
          : currentPage // ignore: cast_nullable_to_non_nullable
              as int,
      isLoadingVotePage: null == isLoadingVotePage
          ? _value.isLoadingVotePage
          : isLoadingVotePage // ignore: cast_nullable_to_non_nullable
              as bool,
      selectedCat: freezed == selectedCat
          ? _value.selectedCat
          : selectedCat // ignore: cast_nullable_to_non_nullable
              as CatEntity?,
      catList: null == catList
          ? _value.catList
          : catList // ignore: cast_nullable_to_non_nullable
              as List<CatEntity>,
      isLoadingSearchPage: null == isLoadingSearchPage
          ? _value.isLoadingSearchPage
          : isLoadingSearchPage // ignore: cast_nullable_to_non_nullable
              as bool,
      pageController: null == pageController
          ? _value.pageController
          : pageController // ignore: cast_nullable_to_non_nullable
              as PageController,
      catBreedVote: freezed == catBreedVote
          ? _value.catBreedVote
          : catBreedVote // ignore: cast_nullable_to_non_nullable
              as CatBreedVoteEntity?,
      imagesCatList: null == imagesCatList
          ? _value.imagesCatList
          : imagesCatList // ignore: cast_nullable_to_non_nullable
              as List<ImageCatEntity>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$HomeStateImplCopyWith<$Res>
    implements $HomeStateCopyWith<$Res> {
  factory _$$HomeStateImplCopyWith(
          _$HomeStateImpl value, $Res Function(_$HomeStateImpl) then) =
      __$$HomeStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool isLoading,
      int currentPage,
      bool isLoadingVotePage,
      CatEntity? selectedCat,
      List<CatEntity> catList,
      bool isLoadingSearchPage,
      PageController pageController,
      CatBreedVoteEntity? catBreedVote,
      List<ImageCatEntity> imagesCatList});
}

/// @nodoc
class __$$HomeStateImplCopyWithImpl<$Res>
    extends _$HomeStateCopyWithImpl<$Res, _$HomeStateImpl>
    implements _$$HomeStateImplCopyWith<$Res> {
  __$$HomeStateImplCopyWithImpl(
      _$HomeStateImpl _value, $Res Function(_$HomeStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isLoading = null,
    Object? currentPage = null,
    Object? isLoadingVotePage = null,
    Object? selectedCat = freezed,
    Object? catList = null,
    Object? isLoadingSearchPage = null,
    Object? pageController = null,
    Object? catBreedVote = freezed,
    Object? imagesCatList = null,
  }) {
    return _then(_$HomeStateImpl(
      isLoading: null == isLoading
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      currentPage: null == currentPage
          ? _value.currentPage
          : currentPage // ignore: cast_nullable_to_non_nullable
              as int,
      isLoadingVotePage: null == isLoadingVotePage
          ? _value.isLoadingVotePage
          : isLoadingVotePage // ignore: cast_nullable_to_non_nullable
              as bool,
      selectedCat: freezed == selectedCat
          ? _value.selectedCat
          : selectedCat // ignore: cast_nullable_to_non_nullable
              as CatEntity?,
      catList: null == catList
          ? _value._catList
          : catList // ignore: cast_nullable_to_non_nullable
              as List<CatEntity>,
      isLoadingSearchPage: null == isLoadingSearchPage
          ? _value.isLoadingSearchPage
          : isLoadingSearchPage // ignore: cast_nullable_to_non_nullable
              as bool,
      pageController: null == pageController
          ? _value.pageController
          : pageController // ignore: cast_nullable_to_non_nullable
              as PageController,
      catBreedVote: freezed == catBreedVote
          ? _value.catBreedVote
          : catBreedVote // ignore: cast_nullable_to_non_nullable
              as CatBreedVoteEntity?,
      imagesCatList: null == imagesCatList
          ? _value._imagesCatList
          : imagesCatList // ignore: cast_nullable_to_non_nullable
              as List<ImageCatEntity>,
    ));
  }
}

/// @nodoc

class _$HomeStateImpl extends _HomeState {
  const _$HomeStateImpl(
      {required this.isLoading,
      required this.currentPage,
      required this.isLoadingVotePage,
      required this.selectedCat,
      required final List<CatEntity> catList,
      required this.isLoadingSearchPage,
      required this.pageController,
      required this.catBreedVote,
      required final List<ImageCatEntity> imagesCatList})
      : _catList = catList,
        _imagesCatList = imagesCatList,
        super._();

  @override
  final bool isLoading;
  @override
  final int currentPage;
  @override
  final bool isLoadingVotePage;
  @override
  final CatEntity? selectedCat;
  final List<CatEntity> _catList;
  @override
  List<CatEntity> get catList {
    if (_catList is EqualUnmodifiableListView) return _catList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_catList);
  }

  @override
  final bool isLoadingSearchPage;
  @override
  final PageController pageController;
  @override
  final CatBreedVoteEntity? catBreedVote;
  final List<ImageCatEntity> _imagesCatList;
  @override
  List<ImageCatEntity> get imagesCatList {
    if (_imagesCatList is EqualUnmodifiableListView) return _imagesCatList;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_imagesCatList);
  }

  @override
  String toString() {
    return 'HomeState(isLoading: $isLoading, currentPage: $currentPage, isLoadingVotePage: $isLoadingVotePage, selectedCat: $selectedCat, catList: $catList, isLoadingSearchPage: $isLoadingSearchPage, pageController: $pageController, catBreedVote: $catBreedVote, imagesCatList: $imagesCatList)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HomeStateImpl &&
            (identical(other.isLoading, isLoading) ||
                other.isLoading == isLoading) &&
            (identical(other.currentPage, currentPage) ||
                other.currentPage == currentPage) &&
            (identical(other.isLoadingVotePage, isLoadingVotePage) ||
                other.isLoadingVotePage == isLoadingVotePage) &&
            (identical(other.selectedCat, selectedCat) ||
                other.selectedCat == selectedCat) &&
            const DeepCollectionEquality().equals(other._catList, _catList) &&
            (identical(other.isLoadingSearchPage, isLoadingSearchPage) ||
                other.isLoadingSearchPage == isLoadingSearchPage) &&
            (identical(other.pageController, pageController) ||
                other.pageController == pageController) &&
            (identical(other.catBreedVote, catBreedVote) ||
                other.catBreedVote == catBreedVote) &&
            const DeepCollectionEquality()
                .equals(other._imagesCatList, _imagesCatList));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      isLoading,
      currentPage,
      isLoadingVotePage,
      selectedCat,
      const DeepCollectionEquality().hash(_catList),
      isLoadingSearchPage,
      pageController,
      catBreedVote,
      const DeepCollectionEquality().hash(_imagesCatList));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$HomeStateImplCopyWith<_$HomeStateImpl> get copyWith =>
      __$$HomeStateImplCopyWithImpl<_$HomeStateImpl>(this, _$identity);
}

abstract class _HomeState extends HomeState {
  const factory _HomeState(
      {required final bool isLoading,
      required final int currentPage,
      required final bool isLoadingVotePage,
      required final CatEntity? selectedCat,
      required final List<CatEntity> catList,
      required final bool isLoadingSearchPage,
      required final PageController pageController,
      required final CatBreedVoteEntity? catBreedVote,
      required final List<ImageCatEntity> imagesCatList}) = _$HomeStateImpl;
  const _HomeState._() : super._();

  @override
  bool get isLoading;
  @override
  int get currentPage;
  @override
  bool get isLoadingVotePage;
  @override
  CatEntity? get selectedCat;
  @override
  List<CatEntity> get catList;
  @override
  bool get isLoadingSearchPage;
  @override
  PageController get pageController;
  @override
  CatBreedVoteEntity? get catBreedVote;
  @override
  List<ImageCatEntity> get imagesCatList;
  @override
  @JsonKey(ignore: true)
  _$$HomeStateImplCopyWith<_$HomeStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
