import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';

import 'injection_container.config.dart';

final getIt = GetIt.instance;

@InjectableInit()
void injectDependencies() => getIt.init();

@module
abstract class InjectableModule {
  @lazySingleton
  http.Client get ht => http.Client();
}
