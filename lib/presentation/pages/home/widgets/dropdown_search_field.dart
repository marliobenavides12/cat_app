import 'package:flutter/material.dart';

import 'package:dropdown_search/dropdown_search.dart';

import '../../../blocs/home/home_bloc.dart';
import '../../../../domain/entities/cat_entity.dart';

class DropdownSearchField extends StatelessWidget {
  const DropdownSearchField({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<HomeBloc>().state;

    return DropdownSearch<CatEntity>(
      items: state.catList,
      enabled: !state.isLoading,
      selectedItem: state.selectedCat,
      popupProps: PopupProps.modalBottomSheet(
        fit: FlexFit.loose,
        showSearchBox: true,
        showSelectedItems: false,
        searchFieldProps: TextFieldProps(
          padding: const EdgeInsets.all(10),
          autofocus: true,
          decoration: InputDecoration(
            hintText: 'Buscar',
            border: _inputBorder(Colors.blue),
            errorBorder: _inputBorder(Colors.red),
            enabledBorder: _inputBorder(Colors.blue),
            contentPadding: const EdgeInsets.only(left: 10),
          ),
          // restorationId: 'kdkdk'
        ),
        searchDelay: const Duration(milliseconds: 500),
        itemBuilder: (context, item, isSelected) => ListTile(
          dense: true,
          minVerticalPadding: 4,
          title: Text(item.name.toString()),
        ),
      ),
      dropdownDecoratorProps: DropDownDecoratorProps(
        dropdownSearchDecoration: InputDecoration(
          filled: true,
          isDense: true,
          border: _inputBorder(Colors.blue),
          errorBorder: _inputBorder(Colors.red),
          enabledBorder: _inputBorder(Colors.blue),
          contentPadding: const EdgeInsets.only(left: 10),
        ),
        baseStyle: const TextStyle(decoration: TextDecoration.none),
      ),
      dropdownBuilder: (_, response) => Text(response?.name ?? ''),
      onChanged: (CatEntity? cat) => context.read<HomeBloc>().add(
            HomeEvent.selectedCatEvent(cat),
          ),
    );
  }

  InputBorder _inputBorder(Color color) => OutlineInputBorder(
        borderSide: BorderSide(color: color, width: 2),
        borderRadius: const BorderRadius.all(Radius.circular(8)),
      );
}
