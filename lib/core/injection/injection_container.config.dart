// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:cat_app/core/injection/injection_container.dart' as _i8;
import 'package:cat_app/data/datasources/datasources.dart' as _i4;
import 'package:cat_app/data/repositories/repositorie_impl.dart' as _i6;
import 'package:cat_app/domain/repositories/repository.dart' as _i5;
import 'package:cat_app/domain/use_cases/use_cases.dart' as _i7;
import 'package:get_it/get_it.dart' as _i1;
import 'package:http/http.dart' as _i3;
import 'package:injectable/injectable.dart' as _i2;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final injectableModule = _$InjectableModule();
    gh.lazySingleton<_i3.Client>(() => injectableModule.ht);
    gh.factory<_i4.DataSourceImpl>(() => _i4.UsersDatasource(gh<_i3.Client>()));
    gh.factory<_i5.Repository>(
        () => _i6.RepositoryImpl(gh<_i4.DataSourceImpl>()));
    gh.lazySingleton<_i7.UseCase>(() => _i7.UseCase(gh<_i5.Repository>()));
    return this;
  }
}

class _$InjectableModule extends _i8.InjectableModule {}
