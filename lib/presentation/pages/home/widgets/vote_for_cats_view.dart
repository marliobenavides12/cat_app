import 'package:flutter/material.dart';

import '../../../widgets/loading.dart';
import '../../../blocs/home/home_bloc.dart';
import '../../../../core/constant/constant.dart';

class VoteForCatsView extends StatelessWidget {
  const VoteForCatsView({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<HomeBloc>().state;

    return Expanded(
      child: PageView(
        controller: state.pageController,
        onPageChanged: (value) =>
            context.read<HomeBloc>().add(const HomeEvent.swipeAndLoadEvent()),
        children: [
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: (state.isLoadingVotePage)
                  ? [const Expanded(child: Center(child: Loading()))]
                  : [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '${state.catBreedVote?.cat?.name ?? ''} ',
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Text(
                            '(${state.catBreedVote?.cat?.origin ?? ''})',
                            style: const TextStyle(fontSize: 16),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20.0),
                      Container(
                        width: 360,
                        height: 260,
                        clipBehavior: Clip.antiAlias,
                        decoration: BoxDecoration(
                          // color: Colors.red,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Image(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                            state.catBreedVote?.imageCat?.url ?? '',
                          ),
                        ),
                      ),
                      const SizedBox(height: 20.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          IconButton(
                            onPressed: () => context.read<HomeBloc>().add(
                                  const HomeEvent.voteForCatBreedEvent(
                                    VoteType.like,
                                  ),
                                ),
                            icon: const Image(
                              height: 36,
                              image: AssetImage('assets/icons/i_like.png'),
                            ),
                          ),
                          IconButton(
                            onPressed: () => context.read<HomeBloc>().add(
                                  const HomeEvent.voteForCatBreedEvent(
                                    VoteType.doesNotLike,
                                  ),
                                ),
                            icon: const Image(
                              height: 36,
                              image: AssetImage('assets/icons/i_dont_like.png'),
                            ),
                          ),
                        ],
                      ),
                    ],
            ),
          ),
          const SizedBox.shrink(),
        ],
      ),
    );
  }
}
