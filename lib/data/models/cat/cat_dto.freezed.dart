// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'cat_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

CatDto _$CatDtoFromJson(Map<String, dynamic> json) {
  return _CatDto.fromJson(json);
}

/// @nodoc
mixin _$CatDto {
  String? get id => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  String? get origin => throw _privateConstructorUsedError;
  int? get intelligence => throw _privateConstructorUsedError;
  int? get healthIssues => throw _privateConstructorUsedError;
  String? get description => throw _privateConstructorUsedError;
  String? get wikipediaUrl => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CatDtoCopyWith<CatDto> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CatDtoCopyWith<$Res> {
  factory $CatDtoCopyWith(CatDto value, $Res Function(CatDto) then) =
      _$CatDtoCopyWithImpl<$Res, CatDto>;
  @useResult
  $Res call(
      {String? id,
      String? name,
      String? origin,
      int? intelligence,
      int? healthIssues,
      String? description,
      String? wikipediaUrl});
}

/// @nodoc
class _$CatDtoCopyWithImpl<$Res, $Val extends CatDto>
    implements $CatDtoCopyWith<$Res> {
  _$CatDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? origin = freezed,
    Object? intelligence = freezed,
    Object? healthIssues = freezed,
    Object? description = freezed,
    Object? wikipediaUrl = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      origin: freezed == origin
          ? _value.origin
          : origin // ignore: cast_nullable_to_non_nullable
              as String?,
      intelligence: freezed == intelligence
          ? _value.intelligence
          : intelligence // ignore: cast_nullable_to_non_nullable
              as int?,
      healthIssues: freezed == healthIssues
          ? _value.healthIssues
          : healthIssues // ignore: cast_nullable_to_non_nullable
              as int?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      wikipediaUrl: freezed == wikipediaUrl
          ? _value.wikipediaUrl
          : wikipediaUrl // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CatDtoImplCopyWith<$Res> implements $CatDtoCopyWith<$Res> {
  factory _$$CatDtoImplCopyWith(
          _$CatDtoImpl value, $Res Function(_$CatDtoImpl) then) =
      __$$CatDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? id,
      String? name,
      String? origin,
      int? intelligence,
      int? healthIssues,
      String? description,
      String? wikipediaUrl});
}

/// @nodoc
class __$$CatDtoImplCopyWithImpl<$Res>
    extends _$CatDtoCopyWithImpl<$Res, _$CatDtoImpl>
    implements _$$CatDtoImplCopyWith<$Res> {
  __$$CatDtoImplCopyWithImpl(
      _$CatDtoImpl _value, $Res Function(_$CatDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? origin = freezed,
    Object? intelligence = freezed,
    Object? healthIssues = freezed,
    Object? description = freezed,
    Object? wikipediaUrl = freezed,
  }) {
    return _then(_$CatDtoImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      origin: freezed == origin
          ? _value.origin
          : origin // ignore: cast_nullable_to_non_nullable
              as String?,
      intelligence: freezed == intelligence
          ? _value.intelligence
          : intelligence // ignore: cast_nullable_to_non_nullable
              as int?,
      healthIssues: freezed == healthIssues
          ? _value.healthIssues
          : healthIssues // ignore: cast_nullable_to_non_nullable
              as int?,
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      wikipediaUrl: freezed == wikipediaUrl
          ? _value.wikipediaUrl
          : wikipediaUrl // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class _$CatDtoImpl extends _CatDto {
  const _$CatDtoImpl(
      {required this.id,
      required this.name,
      required this.origin,
      required this.intelligence,
      required this.healthIssues,
      required this.description,
      required this.wikipediaUrl})
      : super._();

  factory _$CatDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$CatDtoImplFromJson(json);

  @override
  final String? id;
  @override
  final String? name;
  @override
  final String? origin;
  @override
  final int? intelligence;
  @override
  final int? healthIssues;
  @override
  final String? description;
  @override
  final String? wikipediaUrl;

  @override
  String toString() {
    return 'CatDto(id: $id, name: $name, origin: $origin, intelligence: $intelligence, healthIssues: $healthIssues, description: $description, wikipediaUrl: $wikipediaUrl)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CatDtoImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.origin, origin) || other.origin == origin) &&
            (identical(other.intelligence, intelligence) ||
                other.intelligence == intelligence) &&
            (identical(other.healthIssues, healthIssues) ||
                other.healthIssues == healthIssues) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.wikipediaUrl, wikipediaUrl) ||
                other.wikipediaUrl == wikipediaUrl));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, name, origin, intelligence,
      healthIssues, description, wikipediaUrl);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CatDtoImplCopyWith<_$CatDtoImpl> get copyWith =>
      __$$CatDtoImplCopyWithImpl<_$CatDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$CatDtoImplToJson(
      this,
    );
  }
}

abstract class _CatDto extends CatDto {
  const factory _CatDto(
      {required final String? id,
      required final String? name,
      required final String? origin,
      required final int? intelligence,
      required final int? healthIssues,
      required final String? description,
      required final String? wikipediaUrl}) = _$CatDtoImpl;
  const _CatDto._() : super._();

  factory _CatDto.fromJson(Map<String, dynamic> json) = _$CatDtoImpl.fromJson;

  @override
  String? get id;
  @override
  String? get name;
  @override
  String? get origin;
  @override
  int? get intelligence;
  @override
  int? get healthIssues;
  @override
  String? get description;
  @override
  String? get wikipediaUrl;
  @override
  @JsonKey(ignore: true)
  _$$CatDtoImplCopyWith<_$CatDtoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
