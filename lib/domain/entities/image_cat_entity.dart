import 'package:equatable/equatable.dart';

class ImageCatEntity extends Equatable {
  final String? id;
  final int? width;
  final int? height;
  final String? url;

  const ImageCatEntity({this.id, this.url, this.width, this.height});

  @override
  List<Object?> get props => [id, url, width, height];
}
