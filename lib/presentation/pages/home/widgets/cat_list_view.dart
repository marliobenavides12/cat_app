import 'package:flutter/material.dart';

import '../../../widgets/loading.dart';
import 'cat_information.dart';
import 'dropdown_search_field.dart';
import '../../../blocs/home/home_bloc.dart';

class CatListView extends StatelessWidget {
  const CatListView({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<HomeBloc>().state;

    return Expanded(
      child: Column(
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: DropdownSearchField(),
          ),
          (state.isLoading)
              ? const Expanded(child: Center(child: Loading()))
              : Expanded(
                  child: SingleChildScrollView(
                    child: Center(
                      child: Column(
                        children: [
                          if (state.selectedCat != null)
                            const SizedBox(height: 50.0),
                          if (state.selectedCat != null) const CatInformation(),
                        ],
                      ),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
