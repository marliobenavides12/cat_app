import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../entities/cat_entity.dart';
import '../repositories/repository.dart';
import '../entities/image_cat_entity.dart';

@lazySingleton
class UseCase {
  final Repository repository;

  UseCase(this.repository);

  Future<Either<String, List<CatEntity>>> getCatsList() async =>
      await repository.getCatsList();

  Future<Either<String, List<ImageCatEntity>>> getImagesCat(
    String breedId,
  ) async =>
      await repository.getImagesCat(breedId);

  Future<Either<String, bool>> voteForCatBreed({
    required int vote,
    required String imageId,
  }) async =>
      await repository.voteForCatBreed(vote: vote, imageId: imageId);
}
