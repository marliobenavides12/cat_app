import 'package:dartz/dartz.dart';

import '../entities/cat_entity.dart';
import '../entities/image_cat_entity.dart';

abstract class Repository {
  Future<Either<String, List<CatEntity>>> getCatsList();
  Future<Either<String, List<ImageCatEntity>>> getImagesCat(String breedId);
  Future<Either<String, bool>> voteForCatBreed({
    required int vote,
    required String imageId,
  });
}
