part of 'home_bloc.dart';

@freezed
abstract class HomeState with _$HomeState {
  const HomeState._();

  const factory HomeState({
    required bool isLoading,
    required int currentPage,
    required bool isLoadingVotePage,
    required CatEntity? selectedCat,
    required List<CatEntity> catList,
    required bool isLoadingSearchPage,
    required PageController pageController,
    required CatBreedVoteEntity? catBreedVote,
    required List<ImageCatEntity> imagesCatList,
  }) = _HomeState;

  factory HomeState.initial() => HomeState(
        catList: [],
        currentPage: 0,
        isLoading: true,
        selectedCat: null,
        imagesCatList: [],
        catBreedVote: null,
        isLoadingVotePage: true,
        isLoadingSearchPage: true,
        pageController: PageController(),
      );
}
