import 'package:flutter/material.dart';

import 'package:lottie/lottie.dart';
import 'package:animate_do/animate_do.dart';

import '../home/home_page.dart';
import '../../blocs/splash/splash_bloc.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SplashBloc()..add(const SplashEvent.startedEvent()),
      // value: BlocProvider.of<SplashBloc>(context),
      child: BlocListener<SplashBloc, SplashState>(
        listener: (context, state) {
          if (state.isLoading) {
            context.read()<SplashBloc>().add(const SplashEvent.startedEvent());
          } else {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => const HomePage()),
            );
          }
        },
        child: Scaffold(
          body: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                FadeInUpBig(
                  child: Lottie.asset(
                    'assets/lottie_files/splash_loading_animation.json',
                    width: 260,
                    fit: BoxFit.contain,
                  ),
                ),
              ],
            ),
          ),
        ),
        // child: Scaffold(
        //   body: Stack(
        //     clipBehavior: Clip.none,
        //     alignment: AlignmentDirectional.center,
        //     children: [
        //       Positioned(
        //         top: 100,
        //         child: FadeInDown(
        //           delay: const Duration(seconds: 2),
        //           child: const Text(
        //             'Mis Gatos',
        //             style: TextStyle(
        //               fontSize: 30,
        //               fontWeight: FontWeight.bold,
        //               // color: AppColors.primaryBlue,
        //             ),
        //           ),
        //         ),
        //       ),
        //       Positioned(
        //         bottom: 120,
        //         child: FadeInLeft(
        //           child: Lottie.asset(
        //             'assets/lottie_files/animation_moon.json',
        //             width: 60,
        //             fit: BoxFit.contain,
        //           ),
        //         ),
        //       ),
        //       Center(
        //         child: Column(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: [
        //             Lottie.asset(
        //               'assets/lottie_files/splash_animation_cat.json',
        //               width: 260,
        //               repeat: false,
        //             ),
        //           ],
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
      ),
    );
  }
}
