import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';

import '../../core/constant/environment.dart';
import '../models/cat/cat_dto.dart';
import '../models/image_cat/image_cat_dto.dart';

abstract class DataSourceImpl {
  Future<List<CatDto>> getCatsList();
  Future<List<ImageCatDto>> getImagesCat(String breedId);
  Future<bool> voteForCatBreed({required int vote, required String imageId});
}

@Injectable(as: DataSourceImpl)
class UsersDatasource implements DataSourceImpl {
  final http.Client _httpClient;

  UsersDatasource(this._httpClient);

  @override
  Future<List<CatDto>> getCatsList() async {
    Uri url = Uri.parse('${Env.baseUrl}/breeds');
    var response = await _httpClient.get(
      url,
      headers: {'x-api-key': Env.apiKey, 'Content-Type': 'application/json'},
    );
    var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as List;
    // var uri = Uri.parse(decodedResponse['uri'] as String);
    // print(await _httpClient.get(uri));

    final List<CatDto> catList = (decodedResponse)
        .map((cat) => CatDto.fromJson(cat as Map<String, dynamic>))
        .toList();

    return catList;
  }

  @override
  Future<List<ImageCatDto>> getImagesCat(String breedId) async {
    Uri url =
        Uri.parse('${Env.baseUrl}/images/search?limit=10&breed_ids=$breedId');
    var response = await _httpClient.get(
      url,
      headers: {'x-api-key': Env.apiKey, 'Content-Type': 'application/json'},
    );
    var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as List;

    final List<ImageCatDto> catList = (decodedResponse)
        .map((image) => ImageCatDto.fromJson(image as Map<String, dynamic>))
        .toList();

    return catList;
  }

  @override
  Future<bool> voteForCatBreed({
    required int vote,
    required String imageId,
  }) async {
    Uri url = Uri.parse('${Env.baseUrl}/votes');
    final response = await http.post(
      url,
      body: jsonEncode({'value': vote, 'image_id': imageId}),
      headers: {'x-api-key': Env.apiKey, 'Content-Type': 'application/json'},
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }
}
