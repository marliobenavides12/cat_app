import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../core/constant/constant.dart';
import '../../../domain/entities/cat_entity.dart';
import '../../../domain/use_cases/use_cases.dart';
import '../../../domain/entities/image_cat_entity.dart';
import '../../../domain/entities/cat_breed_vote_entity.dart';

export 'package:flutter_bloc/flutter_bloc.dart';

part 'home_event.dart';
part 'home_state.dart';
part 'home_bloc.freezed.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final UseCase _useCase;

  HomeBloc(
    this._useCase,
  ) : super(HomeState.initial()) {
    on<_CurrentPageEvent>(_currentPage);
    on<_SelectedCatEvent>(_selectedCat);
    on<_CleanDataEvent>(_cleanData);

    on<_GetCatListEvent>(_getCatsList);
    on<_GetImagesCatEvent>(_getImagesCat);
    on<_GetRamdomCatBreedEvent>(_getRamdomCatBreed);
    on<_VoteForCatBreedEvent>(_voteForCatBreed);
    on<_SwipeAndLoadEvent>(_swipeAndLoad);
  }

  void _currentPage(_CurrentPageEvent event, Emitter<HomeState> emit) =>
      emit(state.copyWith(currentPage: event.currentPage));

  Future<void> _selectedCat(
    _SelectedCatEvent event,
    Emitter<HomeState> emit,
  ) async {
    emit(state.copyWith(isLoading: true));
    await _getImagesCat(_GetImagesCatEvent(event.selectedCat?.id ?? ''), emit);
    emit(state.copyWith(isLoading: false, selectedCat: event.selectedCat));
  }

  void _cleanData(_CleanDataEvent event, Emitter<HomeState> emit) =>
      emit(state.copyWith(selectedCat: null));

  Future<void> _getCatsList(
    _GetCatListEvent event,
    Emitter<HomeState> emit,
  ) async {
    emit(state.copyWith(isLoading: true));
    final cats = await _useCase.getCatsList();

    cats.fold(
      (failure) => emit(
        state.copyWith(isLoading: false),
      ),
      (List<CatEntity> catList) async {
        emit(state.copyWith(catList: catList, isLoading: false));
        print('OK - GET catList');
        if (catList.isNotEmpty) {
          add(const HomeEvent.getRamdomCatBreedEvent());
        }
      },
    );
  }

  Future<void> _getImagesCat(
    _GetImagesCatEvent event,
    Emitter<HomeState> emit,
  ) async {
    emit(state.copyWith(isLoading: true));
    final cats = await _useCase.getImagesCat(event.breedId);

    cats.fold(
      (failure) => emit(
        state.copyWith(isLoading: false),
      ),
      (List<ImageCatEntity> imagesCatList) {
        emit(state.copyWith(imagesCatList: imagesCatList, isLoading: false));
      },
    );
  }

  Future<void> _getRamdomCatBreed(
    _GetRamdomCatBreedEvent event,
    Emitter<HomeState> emit,
  ) async {
    emit(state.copyWith(isLoadingVotePage: true));
    final int ramdomCatBreed = Random().nextInt(state.catList.length);
    final catBreed = state.catList[ramdomCatBreed];
    final cats = await _useCase.getImagesCat(catBreed.id ?? '');

    cats.fold(
      (failure) => emit(state.copyWith(isLoadingVotePage: false)),
      (List<ImageCatEntity> imagesCatList) {
        final ImageCatEntity imageCat = imagesCatList.firstWhere(
          (element) => element.url.toString().contains('http'),
        );
        final CatBreedVoteEntity catBreedVote = CatBreedVoteEntity(
          cat: catBreed,
          imageCat: imageCat,
        );
        emit(
          state.copyWith(isLoadingVotePage: false, catBreedVote: catBreedVote),
        );
        print(state.isLoadingVotePage);
      },
    );
  }

  Future<void> _voteForCatBreed(
    _VoteForCatBreedEvent event,
    Emitter<HomeState> emit,
  ) async {
    emit(state.copyWith(isLoadingVotePage: true));
    final cats = await _useCase.voteForCatBreed(
      vote: event.voteType == VoteType.like ? 1 : 0,
      imageId: state.catBreedVote?.imageCat?.id ?? '',
    );

    cats.fold(
      (failure) => emit(state.copyWith(isLoadingVotePage: false)),
      (bool value) {
        if (value) {
          add(const HomeEvent.getRamdomCatBreedEvent());
        }
      },
    );
  }

  _swipeAndLoad(
    _SwipeAndLoadEvent event,
    Emitter<HomeState> emit,
  ) {
    emit(state.copyWith(isLoadingVotePage: true));
    state.pageController.animateToPage(
      0,
      curve: Curves.easeInOut,
      duration: const Duration(milliseconds: 300),
    );
    add(const HomeEvent.getRamdomCatBreedEvent());
  }
}
