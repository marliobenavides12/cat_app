import 'package:flutter/material.dart';

import 'widgets/cat_list_view.dart';
import 'widgets/vote_for_cats_view.dart';
import '../../blocs/home/home_bloc.dart';
import '../../../domain/use_cases/use_cases.dart';
import 'package:cat_app/core/injection/injection_container.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
      lazy: false,
      child: const HomeContent(),
      create: (_) => HomeBloc(
        getIt<UseCase>(),
      )..add(const HomeEvent.getCatsListEvent()),
    );
  }
}

class HomeContent extends StatelessWidget {
  const HomeContent({super.key});

  @override
  Widget build(BuildContext context) {
    final state = context.watch<HomeBloc>().state;
    final List<Widget> children = [
      const CatListView(),
      const VoteForCatsView(),
    ];

    return Scaffold(
      appBar: AppBar(
        title: const Text('Gatos'),
        actions: (state.selectedCat != null && state.currentPage == 0)
            ? [
                IconButton(
                  onPressed: () => context
                      .read<HomeBloc>()
                      .add(const HomeEvent.cleanDataEvent()),
                  icon: const Icon(Icons.delete_sweep_outlined),
                )
              ]
            : null,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [children[state.currentPage]],
      ),
      /* body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            children: [
              DropdownSearch<String>(
                items: ["Brazil", "Italia (Disabled)", "Tunisia", 'Canada'],
                popupProps: PopupProps.menu(
                  showSearchBox: true,
                  showSelectedItems: true,
                ),
                // dropdownSearchDecoration: InputDecoration(labelText: "Name"),
                // asyncItems: (String filter) => getData(filter),
                // itemAsString: (UserModel u) => u.userAsString(),
                // onChanged: (UserModel? data) => print(data),
              ),
              CarouselSlider.builder(
                itemCount: 15,
                itemBuilder:
                    (BuildContext context, int itemIndex, int pageViewIndex) =>
                        Container(
                  child: Text(
                    itemIndex.toString(),
                  ),
                ),
                options: CarouselOptions(
                  autoPlay: true,
                  initialPage: 2,
                  aspectRatio: 2.0,
                  viewportFraction: 0.9,
                  enlargeCenterPage: true,
                ),
              ),
              CarouselSlider(
                items: [
                  Text('data'),
                  Text('data'),
                  Text('data'),
                ],
                // carouselController: buttonCarouselController,
                options: CarouselOptions(
                  autoPlay: true,
                  initialPage: 2,
                  aspectRatio: 2.0,
                  viewportFraction: 0.9,
                  enlargeCenterPage: true,
                ),
              ),
            ],
          ),
        ), */
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: state.currentPage,
        onTap: (value) {
          context.read<HomeBloc>().add(HomeEvent.currentPageEvent(value));
          // if (value == 1) {
          //   context.read<HomeBloc>().add(
          //         const HomeEvent.getRamdomCatBreedEvent(),
          //       );
          // }
        },
        items: const [
          BottomNavigationBarItem(
            label: 'Razas',
            icon: Icon(Icons.search),
          ),
          BottomNavigationBarItem(
            label: 'Votación',
            icon: Icon(Icons.swap_vert),
          ),
        ],
      ),
    );
  }
}
