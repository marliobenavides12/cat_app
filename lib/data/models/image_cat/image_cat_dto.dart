import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../domain/entities/image_cat_entity.dart';

part 'image_cat_dto.g.dart';
part 'image_cat_dto.freezed.dart';

@freezed
abstract class ImageCatDto implements _$ImageCatDto {
  const ImageCatDto._();

  @JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
  const factory ImageCatDto({
    required int? width,
    required String? id,
    required int? height,
    required String? url,
  }) = _ImageCatDto;

  ImageCatEntity toEntity() => ImageCatEntity(
        id: id,
        url: url,
        width: width,
        height: height,
      );

  factory ImageCatDto.fromJson(Map<String, dynamic> json) =>
      _$$ImageCatDtoImplFromJson(json);

  factory ImageCatDto.fromEntity(ImageCatEntity entity) {
    return ImageCatDto(
      id: entity.id,
      url: entity.url,
      width: entity.width,
      height: entity.height,
    );
  }
}
