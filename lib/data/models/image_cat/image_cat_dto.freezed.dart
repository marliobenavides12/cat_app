// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'image_cat_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ImageCatDto _$ImageCatDtoFromJson(Map<String, dynamic> json) {
  return _ImageCatDto.fromJson(json);
}

/// @nodoc
mixin _$ImageCatDto {
  int? get width => throw _privateConstructorUsedError;
  String? get id => throw _privateConstructorUsedError;
  int? get height => throw _privateConstructorUsedError;
  String? get url => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ImageCatDtoCopyWith<ImageCatDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ImageCatDtoCopyWith<$Res> {
  factory $ImageCatDtoCopyWith(
          ImageCatDto value, $Res Function(ImageCatDto) then) =
      _$ImageCatDtoCopyWithImpl<$Res, ImageCatDto>;
  @useResult
  $Res call({int? width, String? id, int? height, String? url});
}

/// @nodoc
class _$ImageCatDtoCopyWithImpl<$Res, $Val extends ImageCatDto>
    implements $ImageCatDtoCopyWith<$Res> {
  _$ImageCatDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? width = freezed,
    Object? id = freezed,
    Object? height = freezed,
    Object? url = freezed,
  }) {
    return _then(_value.copyWith(
      width: freezed == width
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as int?,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      height: freezed == height
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as int?,
      url: freezed == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ImageCatDtoImplCopyWith<$Res>
    implements $ImageCatDtoCopyWith<$Res> {
  factory _$$ImageCatDtoImplCopyWith(
          _$ImageCatDtoImpl value, $Res Function(_$ImageCatDtoImpl) then) =
      __$$ImageCatDtoImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int? width, String? id, int? height, String? url});
}

/// @nodoc
class __$$ImageCatDtoImplCopyWithImpl<$Res>
    extends _$ImageCatDtoCopyWithImpl<$Res, _$ImageCatDtoImpl>
    implements _$$ImageCatDtoImplCopyWith<$Res> {
  __$$ImageCatDtoImplCopyWithImpl(
      _$ImageCatDtoImpl _value, $Res Function(_$ImageCatDtoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? width = freezed,
    Object? id = freezed,
    Object? height = freezed,
    Object? url = freezed,
  }) {
    return _then(_$ImageCatDtoImpl(
      width: freezed == width
          ? _value.width
          : width // ignore: cast_nullable_to_non_nullable
              as int?,
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String?,
      height: freezed == height
          ? _value.height
          : height // ignore: cast_nullable_to_non_nullable
              as int?,
      url: freezed == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class _$ImageCatDtoImpl extends _ImageCatDto {
  const _$ImageCatDtoImpl(
      {required this.width,
      required this.id,
      required this.height,
      required this.url})
      : super._();

  factory _$ImageCatDtoImpl.fromJson(Map<String, dynamic> json) =>
      _$$ImageCatDtoImplFromJson(json);

  @override
  final int? width;
  @override
  final String? id;
  @override
  final int? height;
  @override
  final String? url;

  @override
  String toString() {
    return 'ImageCatDto(width: $width, id: $id, height: $height, url: $url)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ImageCatDtoImpl &&
            (identical(other.width, width) || other.width == width) &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.height, height) || other.height == height) &&
            (identical(other.url, url) || other.url == url));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, width, id, height, url);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ImageCatDtoImplCopyWith<_$ImageCatDtoImpl> get copyWith =>
      __$$ImageCatDtoImplCopyWithImpl<_$ImageCatDtoImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ImageCatDtoImplToJson(
      this,
    );
  }
}

abstract class _ImageCatDto extends ImageCatDto {
  const factory _ImageCatDto(
      {required final int? width,
      required final String? id,
      required final int? height,
      required final String? url}) = _$ImageCatDtoImpl;
  const _ImageCatDto._() : super._();

  factory _ImageCatDto.fromJson(Map<String, dynamic> json) =
      _$ImageCatDtoImpl.fromJson;

  @override
  int? get width;
  @override
  String? get id;
  @override
  int? get height;
  @override
  String? get url;
  @override
  @JsonKey(ignore: true)
  _$$ImageCatDtoImplCopyWith<_$ImageCatDtoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
