import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../datasources/datasources.dart';
import '../../domain/entities/cat_entity.dart';
import '../../domain/repositories/repository.dart';
import '../../domain/entities/image_cat_entity.dart';

@Injectable(as: Repository)
class RepositoryImpl implements Repository {
  final DataSourceImpl dataSourceImpl;

  RepositoryImpl(this.dataSourceImpl);

  @override
  Future<Either<String, List<CatEntity>>> getCatsList() async {
    try {
      final response = await dataSourceImpl.getCatsList();
      final List<CatEntity> catList = List<CatEntity>.from(
        response.map((cat) => cat.toEntity()),
      );
      return Right(catList);
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String, List<ImageCatEntity>>> getImagesCat(
    String breedId,
  ) async {
    try {
      final response = await dataSourceImpl.getImagesCat(breedId);
      final List<ImageCatEntity> imagesCatList = List<ImageCatEntity>.from(
        response.map((image) => image.toEntity()),
      );
      return Right(imagesCatList);
    } catch (e) {
      return Left(e.toString());
    }
  }

  @override
  Future<Either<String, bool>> voteForCatBreed({
    required int vote,
    required String imageId,
  }) async {
    try {
      final response = await dataSourceImpl.voteForCatBreed(
        vote: vote,
        imageId: imageId,
      );

      return Right(response);
    } catch (e) {
      return Left(e.toString());
    }
  }
}
