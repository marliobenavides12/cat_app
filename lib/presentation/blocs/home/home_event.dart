part of 'home_bloc.dart';

@freezed
class HomeEvent with _$HomeEvent {
  const factory HomeEvent.startedEvent() = _StartedEvent;
  const factory HomeEvent.cleanDataEvent() = _CleanDataEvent;
  const factory HomeEvent.selectedCatEvent(CatEntity? selectedCat) =
      _SelectedCatEvent;
  const factory HomeEvent.currentPageEvent(int currentPage) = _CurrentPageEvent;

  const factory HomeEvent.getCatsListEvent() = _GetCatListEvent;
  const factory HomeEvent.getImagesCatEvent(String breedId) =
      _GetImagesCatEvent;
  const factory HomeEvent.getRamdomCatBreedEvent() = _GetRamdomCatBreedEvent;
  const factory HomeEvent.voteForCatBreedEvent(VoteType voteType) =
      _VoteForCatBreedEvent;
  const factory HomeEvent.swipeAndLoadEvent() = _SwipeAndLoadEvent;
}
