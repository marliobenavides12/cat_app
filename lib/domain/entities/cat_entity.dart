import 'package:equatable/equatable.dart';

class CatEntity extends Equatable {
  final String? id;
  final String? name;
  final String? origin;
  final int? healthIssues;
  final int? intelligence;
  final String? description;
  final String? wikipediaUrl;

  const CatEntity({
    this.id,
    this.name,
    this.origin,
    this.description,
    this.intelligence,
    this.healthIssues,
    this.wikipediaUrl,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        origin,
        description,
        intelligence,
        healthIssues,
        wikipediaUrl,
      ];
}
